/* globals gauge */
'use strict';
const {
  openBrowser,
  $,
  closeBrowser,
  closeTab,
  goto,
  click,
  waitFor,
  text,
  textBox,
  hover,
  toRightOf,
  link,
  placeholder,
  write,
  into,
  button,
  evaluate,
  setConfig,
  screenshot,
  below,
  scrollUp,
  storage: { localStorage },
  reload
} = require('taiko');
const assert = require('assert');
const { milliSeconds } = require('../src/generic-function-library/utils');
const { getPassword } = require('../src/generic-function-library/authentication');
const { clear } = require('console');
const headless = process.env.headless_chrome.toLowerCase() == 'true' ? true : false;

// Setting the navigation timeout value globally
setConfig({ navigationTimeout: 120000 });


step('Open Browser', async () => {
    try {
      await openBrowser(
        { headless: headless },
        {
          args: [
            '--disable-gpu',
            '--disable-dev-shm-usage',
            '--disable-setuid-sandbox',
            '--no-first-run',
            '--no-sandbox',
            '--no-zygote'
          ]
        }
      );
      await waitFor('3000');
    } catch (e) {
      console.error(e);
    }
  });

  step('Close Browser', async () => {
    try {
      await closeBrowser();
    } catch (e) {
      console.error(e);
    }
  });

  step('Open main page', async () => {
    try {
      gauge.message(`${process.env.au_url}`);
      await goto(`${process.env.au_url}`, { waitForNavigation: true });
    } catch (e) {
      console.error(e);
    }
  });

  step('Click on Login', async () => {
    try {
        
      const loginLinkExists = await text('Login').exists();
      if (await text("Login").exists() == true) {
        gauge.message('Login link is displayed');
        await click("Login", { waitForNavigation: true });
      } else {
        gauge.message('Login link is not displayed');
       
      }
    } catch (e) {
      console.error(e);
      
    }
  });

  step("Click <linkName> link", async (linkName) => {
    try {
        
        const loginLinkExists = await text(linkName).exists();
        if (await text(linkName).exists() == true) {
          gauge.message('<linkName> link is displayed');
          await click(linkName, { waitForNavigation: true });
        } else {
          gauge.message('<linkName> link is not displayed');
         
        }
      } catch (e) {
        console.error(e);
        
      }
  });

  step("Register user with <workEmail> <firstName> <lastName> <password> and verify message <successfullMessage>",async (workEmail,firstName,lastName,password,successfullMessage) => {
    try {
      
        if (await text("Work Email").exists() == true) {
          await write(workEmail,into(textBox(below("Work Email"))));
          await write(firstName,into(textBox(below("First Name"))));
          await write(lastName,into(textBox(below("Last Name"))));
          await write(password,into(textBox(below("Password"))));
          await clickButton("Start Free Trial");
          
        }
      } catch (e) {
        console.error(e);
        
      }
      assert.equal(await text(successfullMessage).exists(),true);
  });

  async function clickButton(buttonName) {
    const buttonExists = await button(buttonName).exists();
    if (buttonExists) {
     await click(buttonName);
    } else {
      gauge.message(`${buttonName} does not exist`);
    }
  }
