import http from 'k6/http';
import encoding from "k6/encoding";
import {constants} from './feature-toggle-constants.js'

export function getOptions() {
  let option1 =  {
    vus:1,
    noConnectionReuse: true,
    noVUConnectionReuse: true,
    duration: '3m',
    setupTimeout: '2m'
  }

  let option2 = {    
    stages: [
      {target: 10, duration: '2m'},      
      {target: 10, duration: '10m'}
    ],
    setupTimeout: '2m'
  }

  let option3 =  {
    vus:11,    
    noConnectionReuse: true,
    noVUConnectionReuse: true,
    duration: '3m',
    setupTimeout: '2m'
  }

  return {
    option1: option1,
    option2: option2,
    option3: option3
  }

}

//Utility function to get okta token
export function getOktaToken(tenant) {
  const oktaEndpoint = constants[tenant].oktaEndpoint
  const oktaUserNameEncoded = constants[tenant].oktaUserNameEncoded
  const oktaPasswordEncoded = constants[tenant].oktaPasswordEncoded
  const payloadUserNameEncoded = constants[tenant].payloadUserNameEncoded
  const payloadPasswordEncoded = constants[tenant].payloadPasswordEncoded
  let oktaUserName = encoding.b64decode(oktaUserNameEncoded)
  let oktaPassword = encoding.b64decode(oktaPasswordEncoded)
  let payloadUserName = encoding.b64decode(payloadUserNameEncoded)
  let payloadPassword = encoding.b64decode(payloadPasswordEncoded)
  let oktaPayload = 'grant_type=password&username=' + payloadUserName + '&password=' + payloadPassword + '&scope=api:invoke'
  let oktaParams = {
      headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + encoding.b64encode(oktaUserName + ':' + oktaPassword)
      },
  }

  let oktaResponse = http.post(oktaEndpoint, oktaPayload, oktaParams)

  return JSON.parse(oktaResponse.body).access_token
}

export function getParamsForFeatureToggle(token) {
  let featureToggleParams = {
    headers: {
      'x-tenant-code': 'Q1UNIT',
      'Authorization': 'Bearer ' + token
    },
  }
  return featureToggleParams
}

export function getParamsForGroupFeatureToggle(token) {
  let featureToggleParams = {
    headers: {
      'x-tenant-code': 'L0XXXX',
      'Authorization': 'Bearer ' + token
    },
  }
  return featureToggleParams
}

export function getParamsForFeatureToggleWithAdditionalHeaders(token) {
  let featureToggleParams = {
    headers: {
      'x-tenant-code': 'Q1UNIT',
      'x-additional-info': 'true',
      'Authorization': 'Bearer ' + token
    },
  }
  return featureToggleParams
}