export const constants = {
    q1unit : {
        featureToggleEndpoint : 'https://q1epcm-ft-gw.epsilonagilityloyalty.com/feature',
        oktaEndpoint : 'https://epsilonclient.oktapreview.com/oauth2/aussdtjxx6AcFaIq90h7/v1/token',
        oktaUserNameEncoded : 'MG9hc2R1djc1OGxPZVRNYjUwaDc=',
        oktaPasswordEncoded : 'UXFfV3I4b3N0Z09WMmJ6UDAyMll3ZVdSdnVKSFhnNWNITV9ycEdldA==',
        payloadUserNameEncoded : 'c3ZuX29rdGFfZXBjY19xMXVuaXRfY29uZmlnX21nbXRfYXBpQGVwc2lsb24uY29t',
        payloadPasswordEncoded : 'SEJ6aUZkQGJANw==',
        validFeatureKey : 'testValidFeatureKey',
        invalidFeatureKey : 'testInvalidFeatureKey',
        validGroupFeatureKey : 'testValidGroupFeatureKey',
        invalidGroupFeatureKey : 'testInvalidGroupFeatureKey'
    },
    q1unit_admin : {
        featureToggleEndpoint : 'https://q1epcm-ft-gw.epsilonagilityloyalty.com/feature',
        oktaEndpoint : 'https://epsilonclient.oktapreview.com/oauth2/aussdtjxx6AcFaIq90h7/v1/token',
        oktaUserNameEncoded : 'MG9hc2R1djc1OGxPZVRNYjUwaDc=',
        oktaPasswordEncoded : 'UXFfV3I4b3N0Z09WMmJ6UDAyMll3ZVdSdnVKSFhnNWNITV9ycEdldA==',
        payloadUserNameEncoded : 'c3ZuX29rdGFfZXBjY19xMXVuaXRfYWRtaW5fY29uZmlnX21nbXRfYXBpQGVwc2lsb24uY29t',
        payloadPasswordEncoded : 'bXc0dExSQGJANw=='
    }
}