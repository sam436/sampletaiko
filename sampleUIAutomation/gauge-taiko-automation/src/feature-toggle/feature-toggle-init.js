import http from 'k6/http';
import { Counter, Trend } from 'k6/metrics';
import { check, group } from 'k6';
import {getOktaToken, getOptions, getParamsForFeatureToggle, getParamsForFeatureToggleWithAdditionalHeaders} from './feature-toggle-utils.js'
import {constants} from './feature-toggle-constants.js'

//1.Init code
const tenant = 'q1unit'
const validFeatureKey = constants[tenant].validFeatureKey
const invalidFeatureKey = constants[tenant].invalidFeatureKey

//Counters to keep track of error count
let featureToggleErrorCounter = new Counter("Feature Toggle Response Invalid Count");
let featureExistsTrend = new Trend("init_featuretoggle_exists_" + tenant)
let featureNotExistsTrend = new Trend("init_featuretoggle_doesnot_exists_" + tenant)
let featureExistsWithAdditionalInfo = new Trend("init_featuretoggle_exists_additionalInfo_" + tenant)
let totalPayloadTrend = new Trend("init_featuretoggle_combined_" + tenant)

//options to configure k6
export const options = getOptions().option3

//2.Setup code to get an okta token and use it for all the tests
export function setup() {
  let adminToken = getOktaToken(tenant + '_admin')
  let featureToggleParamsAdmin = getParamsForFeatureToggle(adminToken)
  let putFeatureBody = {"featureName": validFeatureKey,"isEnabled": true}
  let featureToggleEndpoint = constants[tenant].featureToggleEndpoint

  //Upsert a valid feature key
  let putFeatureToggleResponse = http.put(featureToggleEndpoint, JSON.stringify(putFeatureBody), featureToggleParamsAdmin);
  console.log("putFeatureToggleResponse: " + putFeatureToggleResponse.status)

  //Delete an invalid feature key
  let deleteFeatureToggleResponse = http.del(featureToggleEndpoint + '/' + invalidFeatureKey, null, featureToggleParamsAdmin);
  console.log("deleteFeatureToggleResponse: " + deleteFeatureToggleResponse.status)

  return getOktaToken(tenant)
}

//3.Default function where each vu performs an iteration. Input token data is the data returned from setup()
export default function(token) {
  let featureToggleEndpoint = constants[tenant].featureToggleEndpoint
  let featureToggleParams = getParamsForFeatureToggle(token)
  let featureToggleParamsWithAdditionalInfo = getParamsForFeatureToggleWithAdditionalHeaders(token)

  group('Feature Exists', function() {
    let featureToggleResponse = http.get(featureToggleEndpoint + '/' + validFeatureKey, featureToggleParams);
    let featureExistsDuration = featureToggleResponse.timings.duration
    featureExistsTrend.add(featureExistsDuration)
    totalPayloadTrend.add(featureExistsDuration)

    check(featureToggleResponse, {
      'feature toggle api response status for Feature Exists is 200': (r) => r.status === 200
    }) || featureToggleErrorCounter.add(1);
  })

  group('Feature Doesnot Exist', function() {
    let featureToggleResponse = http.get(featureToggleEndpoint + '/' + invalidFeatureKey, featureToggleParams);
    let featureNotExistsDuration = featureToggleResponse.timings.duration
    featureNotExistsTrend.add(featureNotExistsDuration)
    totalPayloadTrend.add(featureNotExistsDuration)

    check(featureToggleResponse, {
      'feature toggle api response status for Feature Doesnot Exist is 404': (r) => r.status === 404
    }) || featureToggleErrorCounter.add(1);
  })

  group('Feature Exists With AdditionalInfo', function() {
    let featureToggleResponse = http.get(featureToggleEndpoint + '/' + validFeatureKey, featureToggleParamsWithAdditionalInfo);
    let featureExistsDuration = featureToggleResponse.timings.duration
    featureExistsWithAdditionalInfo.add(featureExistsDuration)
    totalPayloadTrend.add(featureExistsDuration)

    check(featureToggleResponse, {
      'feature toggle api response status for Feature Exists is 200': (r) => r.status === 200
    }) || featureToggleErrorCounter.add(1);
  })

}


