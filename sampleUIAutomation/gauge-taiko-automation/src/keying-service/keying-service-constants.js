export const constants = {
    q1unit : {
        realtimeKeyingServiceEndpoint : 'https://q1unit-ks-gw.skynetlowerenv.com/keys',
        oktaEndpoint : 'https://epsilonclient.oktapreview.com/oauth2/ausnmrrztpa30cBOU0h7/v1/token',
        oktaUserNameEncoded : 'MG9hbm1yeWhycm92cFRwZUQwaDc=',
        oktaPasswordEncoded : 'SDUtZExySHlERFRvY0ZkWHowaVBsZ2dtY0NmTXBUZ3FtTWo0U3hzMQ==',
        payloadUserNameEncoded : 'c3ZuX29rdGFfcTF1bml0X2FwaV9nd0BlcHNpbG9uLmNvbQ==',
        payloadPasswordEncoded : 'UE12UFJLak0zSzZxd2hFYw=='
    }
}