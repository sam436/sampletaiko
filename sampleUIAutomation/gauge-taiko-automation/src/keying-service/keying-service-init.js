import http from 'k6/http';
import { Counter, Trend } from 'k6/metrics';
import { check, group } from 'k6';
import {getOktaToken, getPayloads, getOptions, getParamsForKeyingServiceRealtime} from './keying-service-utils.js'
import {constants} from './keying-service-constants.js'

//1.Init code
const tenant = 'q1unit'

//Counters to keep track of error count
let keyingServiceErrorCounter = new Counter("Keying Service Response Invalid Count");
let newPayloadTrend = new Trend("init_new_payload_" + tenant)
let lookupPayloadTrend = new Trend("init_lookup_payload_" + tenant)
let mixedPayloadTrend = new Trend("init_mixed_payload_" + tenant)
let totalPayloadTrend = new Trend("init_total_payload_" + tenant)

//options to configure k6
export const options = getOptions().option1

//2.Setup code to get an okta token and use it for all the tests
export function setup() {
  let token = getOktaToken(tenant)
  return token
}

//3.Default function where each vu performs an iteration. Input token data is the data returned from setup()
export default function(token) {
  let realtimeKeyingServiceEndpoint = constants[tenant].realtimeKeyingServiceEndpoint
  let realtimeKeyingServiceParams = getParamsForKeyingServiceRealtime(token)
  let payloads = getPayloads(16,5)

  group('New payload', function() {
    let realtimeKeyingServicePayload = payloads.payload1
    let realtimeKeyingServiceResponse = http.post(realtimeKeyingServiceEndpoint, realtimeKeyingServicePayload, realtimeKeyingServiceParams);
    let newPayloadDuration = realtimeKeyingServiceResponse.timings.duration
    newPayloadTrend.add(newPayloadDuration)
    totalPayloadTrend.add(newPayloadDuration)

    check(realtimeKeyingServiceResponse, {
      'keying service response status for New payload is 200': (r) => r.status === 200
    }) || keyingServiceErrorCounter.add(1);
  })

  group('Lookup Payload', function() {
    let realtimeKeyingServicePayload = payloads.payload1
    let realtimeKeyingServiceResponse = http.post(realtimeKeyingServiceEndpoint, realtimeKeyingServicePayload, realtimeKeyingServiceParams);
    let lookupPayloadDuration = realtimeKeyingServiceResponse.timings.duration
    lookupPayloadTrend.add(lookupPayloadDuration)
    totalPayloadTrend.add(lookupPayloadDuration)

    check(realtimeKeyingServiceResponse, {
      'keying service response status for Lookup Payload is 200': (r) => r.status === 200
    }) || keyingServiceErrorCounter.add(1);
  })

  group('Mixed Payload', function() {
    let realtimeKeyingServicePayload = payloads.payload2
    let realtimeKeyingServiceResponse = http.post(realtimeKeyingServiceEndpoint, realtimeKeyingServicePayload, realtimeKeyingServiceParams);
    let mixedPayloadDuration = realtimeKeyingServiceResponse.timings.duration
    mixedPayloadTrend.add(mixedPayloadDuration)
    totalPayloadTrend.add(mixedPayloadDuration)

    check(realtimeKeyingServiceResponse, {
      'keying service response status for Mixed Payload is 200': (r) => r.status === 200
    }) || keyingServiceErrorCounter.add(1);
  })

}


