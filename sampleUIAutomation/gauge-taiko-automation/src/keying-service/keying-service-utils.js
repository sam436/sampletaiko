import http from 'k6/http';
import encoding from "k6/encoding";
import faker from "https://cdnjs.cloudflare.com/ajax/libs/Faker/3.1.0/faker.min.js"
import {constants} from './keying-service-constants.js'

export function getOptions() {
  let option1 =  {
    vus:100,
    iterations:100,
    noConnectionReuse: true,
    noVUConnectionReuse: true,
    setupTimeout: '30s'
  }

  let option2 = {
    stages: [
      {target: 1, duration: '15s'},
      {target: 10, duration: '15s'},
      {target: 25, duration: '15s'},
      {target: 50, duration: '15s'},

    ],
    setupTimeout: '30s'
  }

  return {
    option1: option1,
    option2: option2
  }

}

//Utility function to get okta token
export function getOktaToken(tenant) {
  const oktaEndpoint = constants[tenant].oktaEndpoint
  const oktaUserNameEncoded = constants[tenant].oktaUserNameEncoded
  const oktaPasswordEncoded = constants[tenant].oktaPasswordEncoded
  const payloadUserNameEncoded = constants[tenant].payloadUserNameEncoded
  const payloadPasswordEncoded = constants[tenant].payloadPasswordEncoded
  let oktaUserName = encoding.b64decode(oktaUserNameEncoded)
  let oktaPassword = encoding.b64decode(oktaPasswordEncoded)
  let payloadUserName = encoding.b64decode(payloadUserNameEncoded)
  let payloadPassword = encoding.b64decode(payloadPasswordEncoded)
  let oktaPayload = 'grant_type=password&username=' + payloadUserName + '&password=' + payloadPassword + '&scope=api:invoke'
  let oktaParams = {
      headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + encoding.b64encode(oktaUserName + ':' + oktaPassword)
      },
  };

  let oktaResponse = http.post(oktaEndpoint, oktaPayload, oktaParams);

  return JSON.parse(oktaResponse.body).access_token
}

//Function to get two payloads for keying service realtime.
//numberOfKeys: Totol numbers of keys required for payload Default 16
//numberOfNewKeys: Number of new keys in the Mixed payload Default 5
export function getPayloads(numberOfKeys=16, numberOfNewKeys=5) {
  //New payload
  let payload1 = new Object()
  payload1.keyData = new Array()
  for(let i = 0; i < numberOfKeys; i++) {
    let pair = new Object()
    pair.firstName = faker.name.firstName()
    pair.lastName = faker.name.lastName()
    pair.profileId = faker.random.number().toString()
    pair.phoneNumber = faker.phone.phoneNumber()
    pair.address = faker.address.streetAddress()
    payload1.keyData.push(pair)
  }

  //Mixed payload. First two keys are same and rest of the keys are modified
  let payload2 = new Object()
  payload2.keyData = new Array()
  for(let i = 0; i < numberOfKeys; i++) {
    let pair = new Object()
    pair.firstName = payload1.keyData[i].firstName
    pair.lastName = payload1.keyData[i].lastName
    pair.profileId = payload1.keyData[i].profileId
    pair.phoneNumber = payload1.keyData[i].phoneNumber
    pair.address = payload1.keyData[i].address
    if(i >= numberOfKeys-numberOfNewKeys) {
      pair.email = faker.internet.email()
    }
    payload2.keyData.push(pair)
  }

  return {
    payload1: JSON.stringify(payload1),
    payload2: JSON.stringify(payload2)
  }
}

export function getParamsForKeyingServiceRealtime(token) {
  let realtimeKeyingServiceParams = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
  }
  return realtimeKeyingServiceParams
}