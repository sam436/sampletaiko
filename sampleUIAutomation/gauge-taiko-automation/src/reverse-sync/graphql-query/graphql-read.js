const axios = require('axios');
const _ = require('lodash');
const { promisify } = require('util');
const path = require('path');
const kafkaInputFile = path.resolve('../../../reports/k6');
const fs = require('fs');
const readFileAsync = promisify(fs.readFile);

const profileV5Read = () => {
  return new Promise((resolve, reject) => {
    const query = `
    {
       ProfileV5s(filter: {REASON: "${process.env.GO_PIPELINE_COUNTER}"}){
          PROFILE_ID
        }
     } `;

    // HTTP request
    axios
      .post(
        'http://q1unit-rt.skynetlowerenv.com/execute',
        {
          // GraphQL query
          query
        },
        { headers: { 'Content-Type': 'application/json' } }
      )
      .then(({ data }) => {
        if (data && data.resultCode && data.resultCode === 200 && !_.isEmpty(data.data)) {
          const records = _.get(data.data, 'ProfileV5s', []);
          resolve(records.length);
        } else {
          resolve(0);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
};

(async () => {
  try {
    let producerData;
    const reportPath = `${kafkaInputFile}/kafka-producer-report.json`;
    console.time('Sync time');
    let keepGoing = true;
    while (keepGoing === true) {
      const actualRecordsCount = await profileV5Read();
      try {
        const rawData = await readFileAsync(reportPath)
        producerData = JSON.parse(rawData);
      } catch (e) {
        console.error(`An error occurred while accessing the file`, e);
      }
      const expectedRecordsCount = _.get(producerData, 'totalRecords', 0);
      if (Number(actualRecordsCount) === Number(expectedRecordsCount)) {
        keepGoing = false;
        break;
      } else {
        console.log(`Actual records count: ${actualRecordsCount}, Expected records count: ${expectedRecordsCount}`);
      }
    }
    console.log(`All the messages in Kafka topic were loaded to Redshift!`);
    console.timeEnd('Sync time');
  } catch (e) {
    console.error(`An error occurred while getting the data for the fields`, e);
  }
})();
