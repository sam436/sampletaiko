const kafka = require('kafka-node');
const config = require('./kafka-config');

try {
  const client = new kafka.KafkaClient({ kafkaHost: config.kafka_server });
  let consumer = new kafka.Consumer(client, [{ topic: config.kafka_topic, partition: 0 }], {
    autoCommit: true,
    fetchMaxWaitMs: 1000,
    fetchMaxBytes: 1024 * 1024,
    encoding: 'utf8',
    fromOffset: false
  });
  consumer.on('message', async function (message) {
    if (message && message.topic === config.kafka_topic) {
      console.log(`[kafka-consumer -> ${config.kafka_topic}]: ${JSON.stringify(message.value)}`);
    }
  });
  consumer.on('error', function (err) {
    console.log('error', err);
  });
} catch (e) {
  console.log(e);
}
