const json2html = require('node-json2html');
const fs = require('fs-extra')

let template_table_header = {
    "<>": "tr", "html": [
        {"<>": "th", "html": "No of Kafka Partitions"},
        {"<>": "th", "html": "Hbase Configuration"},
        {"<>": "th", "html": "Real Time Configuration"},
        {"<>": "th", "html": "Time to Consume MDL Messages (Secs)"},
        {"<>": "th", "html": "Time to finish Indexing (Secs)"},
        {"<>": "th", "html": "Total No of Messages"},
        {"<>": "th", "html": "Total No of Records"},
		{"<>": "th", "html": "Records Consumed per Second"},
        {"<>": "th", "html": "Total time (Secs)"}
    ]
}

let template_table_body = {
    "<>": "tr", "html": [
        {"<>": "td", "html": "${KAFKA_PARTITIONS}"},
        {"<>": "td", "html": "${HBASE_CONFIGURATION}"},
        {"<>": "td", "html": "${RT_CONFIGURATION}"},
        {"<>": "td", "html": "${MDL_TOPIC_TIME}"},
        {"<>": "td", "html": "${INDEXER_TOPIC_TIME}"},
        {"<>": "td", "html": "${TOTAL_MESSAGES}"},
        {"<>": "td", "html": "${TOTAL_RECORDS}"},
        {"<>": "td", "html": "${TPS}"},
        {"<>": "td", "html": "${TIME_IN_SECONDS}"}
    ]
}

writeHtmlFromScoresJson("testInfo.json", 'testSummary.html')

function writeHtmlFromScoresJson(jsonFile, htmlTableFile) {
    let data = fs.readJsonSync(jsonFile);

    let table_header = json2html.transform(data[0], template_table_header);
    let table_body = json2html.transform(data, template_table_body);

    let header = '<!DOCTYPE html>' + '<html lang="en">\n' + '<head><style>th {text-align: center;}td {text-align: center;}</style><title>Reverse Synch Report</title></head>'
    let body = '<h1>Reverse Synch Report</h1><br><table id="my_table">\n<table border=1>\n<thead style="background-color:Yellow;">' + table_header + '\n</thead>\n<tbody>\n' + table_body + '\n</tbody>\n</table>'
    body = '<body>' + body + '</body>'

    let html = header + body + '</html>';

    fs.writeFile(htmlTableFile, html, (err) => {
        if (err) throw err;
    });
}
