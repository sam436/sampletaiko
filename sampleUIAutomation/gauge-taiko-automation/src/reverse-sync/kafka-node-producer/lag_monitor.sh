
declare -i mdl_lag=0
declare -i index_lags=0
checkLag(){
mdl_lag=`kafka-consumer-groups --bootstrap-server $KAFKA_SERVER  --describe -group "${TENANTCODE}_MDL" | awk '{SUM+=$6}END{print SUM}'`
indexer_lag=`kafka-consumer-groups --bootstrap-server $KAFKA_SERVER  --describe -group "${TENANTCODE}_realtime_indexer" | awk '{SUM+=$6}END{print SUM}'`
indexer_detail_lag=`kafka-consumer-groups --bootstrap-server $KAFKA_SERVER  --describe -group "${TENANTCODE}_realtime_indexer_detail" | awk '{SUM+=$6}END{print SUM}'`
index_lags=$(( ($indexer_detail_lag+$indexer_lag) ))
}

while :
do
	echo Checking lag
	checkLag
	if [ $index_lags == 0 ] && [ $mdl_lag == 0 ];
    then
       echo "Lag is zero"
       if [ "$mdl_time" -eq "0" ]
       then
          mdl_time=$(date +%s%3N)
         echo "MDL lag end time: $mdl_time"
       fi
	   break
	elif [ $index_lags != 0 ] && [ $mdl_lag == 0 ] && [ "$mdl_time" -eq "0" ];
	then
	    mdl_time=$(date +%s%3N)
	    echo "MDL lag end time: $mdl_time"
    else
       echo "MDL lag is $mdl_lag Indexer lag is $index_lags"
	   sleep 30
    fi
done

