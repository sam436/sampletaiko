const kafka = require('kafka-node');
const config = require('./kafka-config');
const faker = require('faker');
const bluebird = require('bluebird');
const fs = require('fs');
const RECORDS_COUNT = 100;
const readline = require('readline');

let counter = 0;

let array = [];

const generateMessages = async () => {
  return new Promise((resolve, reject) => {
    try {
      let totalMessages = 0;
      let errorMessages = 0;
      let successMessages = 0;
      let totalRecords = 0;
      const client = new kafka.KafkaClient({ kafkaHost: config.kafka_server });
      const producer = new kafka.Producer(client, { partitionerType: 2 });
      const kafkaProducerSend = bluebird.promisify(producer.send, {
        context: producer
      });
      const kafkaProducerClose = bluebird.promisify(producer.close, {
        context: producer
      });
      producer.on('ready', async () => {
        try {
          let keepGoing = true;
          while (keepGoing === true) {
            totalMessages = successMessages + errorMessages;
            let insertMessages = Number(process.env.NUMBER_OF_MESSAGES / RECORDS_COUNT) * 0.7;
            let updateMessages = Number(process.env.NUMBER_OF_MESSAGES / RECORDS_COUNT) * 0.3;
            console.log("Total inserts: " + insertMessages)
            console.log("Total updates: " + updateMessages)
            if (totalMessages === Number(insertMessages + updateMessages)) {
              keepGoing = false;
              break;
            }

            if (totalMessages === Number(insertMessages)) {
              array = await processLineByLine();
            }
            try {
              let payload;
              if (totalMessages < Number(insertMessages)) {
                payload = generatePayload();
              } else {
                //console.log("In Update")
                payload = generatePayloadFromFile(array);
                //console.log("Counter: " + counter)
              }
              for (let message of payload) { // You can use `let` instead of `const` if you like
                await kafkaProducerSend([message]);
                //console.log(`[kafka-producer -> ${config.kafka_topic}]: broker update success`);
              }
              successMessages++;
              //console.log(successMessages)
            } catch (e) {
              errorMessages++;
              //console.log(`[kafka-producer -> ${config.kafka_topic}]: broker update failed` + e);
            }
          }
          await kafkaProducerClose();
          //console.log(`[kafka-producer -> ${config.kafka_topic}]: automated message generation completed`);
          totalRecords = successMessages * RECORDS_COUNT;
          const stats = {
            totalMessages,
            totalRecords,
            successMessages,
            errorMessages
          };
         // console.log(`[kafka-producer -> ${config.kafka_topic}]: total number of messages produced`, stats);
          resolve(stats);
        } catch (error) {
          console.log(`[kafka-producer -> ${config.kafka_topic}]: error occurred in producer`);
          reject(error);
        }
      });

      producer.on('error', (err) => {
        console.log(`[kafka-producer -> ${config.kafka_topic}]: connection errored`);
        throw err;
      });
    } catch (err) {
      console.log(err);
      reject(err);
    }
  });
};



const generatePayload = () => {
  let records_contact = [];
  let records_response = [];
  let records_opt = [];
  let records_surveyResponse = [];
  //console.log("Generate payload")
  for (let i = 0; i < RECORDS_COUNT; i++) {
    var contact = generateRecordContact();
    records_contact.push(contact);
    //obj.a
    var response = generateRecordResponse(contact.ACCT_SRC_CD, contact.ACCT_SRC_NBR, contact.BRAND_CD);
    records_response.push(response);
    var opt = generateRecordOpt(contact.ACCT_SRC_CD, contact.ACCT_SRC_NBR, contact.BRAND_CD, response.EXTERNAL_RESPONSE_ID);
    records_opt.push(opt);
    var surveyResponse = generateRecordSurveyResponse(contact.ACCT_SRC_CD, contact.ACCT_SRC_NBR, contact.BRAND_CD, response.EXTERNAL_RESPONSE_ID);
    records_surveyResponse.push(surveyResponse);
    WriteToFile(contact.ACCT_SRC_CD + "," + contact.ACCT_SRC_NBR + "," + contact.BRAND_CD + "," + response.EXTERNAL_RESPONSE_ID + "," + opt.PROGRAM_CD + "," + surveyResponse.QUESTION_CD + "," + surveyResponse.SURVEY_CD + "\r\n");

  }
  const CONTACT = {
    entityName: 'CONTACT_P8',
    records: records_contact
  };
  const RESPONSE = {
    entityName: 'CONTACT_P8_RESPONSE',
    records: records_response
  };
  const OPT = {
    entityName: 'CONTACT_P8_RESPONSE_OPT',
    records: records_opt
  };
  const SURVEYRESPONSE = {
    entityName: 'CONTACT_P8_RESPONSE_SURVEYRESPONSE',
    records: records_surveyResponse
  };
  return [
    {
      topic: config.kafka_topic,
      messages: JSON.stringify(CONTACT)
    },
    {
      topic: config.kafka_topic,
      messages: JSON.stringify(RESPONSE)
    },
    {
      topic: config.kafka_topic,
      messages: JSON.stringify(OPT)
    }, {
      topic: config.kafka_topic,
      messages: JSON.stringify(SURVEYRESPONSE)
    }
  ];
};


const generatePayloadFromFile = (array) => {
  let records_contact = [];
  let records_response = [];
  let records_opt = [];
  let records_surveyResponse = [];
  //console.log("Generate payload");
  for (let i = 0; i < RECORDS_COUNT; i++) {
    var contact = generateRecordContactFile(array[counter][0], array[counter][1], array[counter][2]);
    records_contact.push(contact);
    var response = generateRecordResponseFile(contact.ACCT_SRC_CD, contact.ACCT_SRC_NBR, contact.BRAND_CD, array[counter][3]);
    records_response.push(response);
    var opt = generateRecordOptFile(contact.ACCT_SRC_CD, contact.ACCT_SRC_NBR, contact.BRAND_CD, response.EXTERNAL_RESPONSE_ID, array[counter][4]);
    records_opt.push(opt);
    var surveyResponse = generateRecordSurveyResponseFile(contact.ACCT_SRC_CD, contact.ACCT_SRC_NBR, contact.BRAND_CD, response.EXTERNAL_RESPONSE_ID, array[counter][5], array[counter][6]);
    records_surveyResponse.push(surveyResponse);
    counter++;
  }
  const CONTACT = {
    entityName: 'CONTACT_P8',
    records: records_contact
  };
  const RESPONSE = {
    entityName: 'CONTACT_P8_RESPONSE',
    records: records_response
  };
  const OPT = {
    entityName: 'CONTACT_P8_RESPONSE_OPT',
    records: records_opt
  };
  const SURVEYRESPONSE = {
    entityName: 'CONTACT_P8_RESPONSE_SURVEYRESPONSE',
    records: records_surveyResponse
  };
  return [
    {
      topic: config.kafka_topic,
      messages: JSON.stringify(CONTACT)
    },
    {
      topic: config.kafka_topic,
      messages: JSON.stringify(RESPONSE)
    },
    {
      topic: config.kafka_topic,
      messages: JSON.stringify(OPT)
    }, {
      topic: config.kafka_topic,
      messages: JSON.stringify(SURVEYRESPONSE)
    }
  ];
};


function WriteToFile(txtString) {
  const fsLibrary = require('fs')
  fsLibrary.appendFile('keys.txt', txtString, (error) => {

    // In case of a error throw err exception.
    if (error) throw err;
  })
}

const generateRecordContact = () => {
  return {
    ACCT_SRC_CD: `ASC_${getUniqueId().substr(0, 10)}`,
    ACCT_SRC_NBR: `ASN_${getUniqueId().substr(0, 10)}`,
    BRAND_CD: `BC_${getUniqueId().substr(0, 10)}`,
    AC_RTN_CD: 'CODE',
    AC_VALID_EMAIL_IND: 'Y',
    ADDR_LINE_1: 'ADDR',
    ADDR_LINE_2: 'ADDR',
    AE_RTN_CD: 'CODE',
    BUSN_NM: 'NM',
    CELL_PHONE_NBR: 'NBR',
    CITY_NM: 'NBR',
    CONSUMER_TYPE: 'TYPE',
    CONTACT_TS: '2010-10-10',
    CONTACT_TYPE_CD: 'CODE',
    CORRELATION_ID: 'ID',
    COUNTRY_CD: 'CODE',
    DATE_OF_BIRTH: '2010-10-10',
    EMAIL_ADDR: 'ADDR',
    EXCLUSION_CD: 'CODE',
    FIRST_NM: 'NAME',
    GENDER_CD: 'CODE',
    HASH_INDIV_ID: 'ID',
    HOME_PHONE_NBR: 'NBR',
    INDIV_ID: 'ID',
    LANG_CD: 'EN',
    LAST_NM: 'NAME',
    MIDDLE_NM: 'NAME',
    NAME_SUFFIX: 'SUFFIX',
    NPI_ID: 'ID',
    POSTAL_CD: 'CODE',
    POSTAL_TYPE_CD: 'CODE',
    PREFIX: 'PREFIX',
    PRESCRIBER_IND: 'IND',
    PRIMARY_SPECIALTY_CD: 'CODE',
    PROVIDER_TYPE: 'TYPE',
    REC_SRC_ORIG_TS: '2010-10-10',
    SCREENING_STATUS: 'STATUS',
    STATE_CD: 'CODE',
    TITLE: 'TITLE',
    WORK_PHONE_NBR: 'NBR'
  };
};


const generateRecordContactFile = (ACCT_SRC_CD, ACCT_SRC_NBR, BRAND_CD) => {
  return {
    ACCT_SRC_CD: ACCT_SRC_CD,
    ACCT_SRC_NBR: ACCT_SRC_NBR,
    BRAND_CD: BRAND_CD,
    AC_RTN_CD: 'CODE',
    AC_VALID_EMAIL_IND: 'Y',
    ADDR_LINE_1: 'ADDR',
    ADDR_LINE_2: 'ADDR',
    AE_RTN_CD: 'CODE',
    BUSN_NM: 'NM',
    CELL_PHONE_NBR: 'NBR',
    CITY_NM: 'NBR',
    CONSUMER_TYPE: 'TYPE',
    CONTACT_TS: '2010-10-10',
    CONTACT_TYPE_CD: 'CODE',
    CORRELATION_ID: 'ID',
    COUNTRY_CD: 'CODE',
    DATE_OF_BIRTH: '2010-10-10',
    EMAIL_ADDR: 'ADDR',
    EXCLUSION_CD: 'CODE',
    FIRST_NM: 'NAME',
    GENDER_CD: 'CODE',
    HASH_INDIV_ID: 'ID',
    HOME_PHONE_NBR: 'NBR',
    INDIV_ID: 'ID',
    LANG_CD: 'EN',
    LAST_NM: 'NAME',
    MIDDLE_NM: 'NAME',
    NAME_SUFFIX: 'SUFFIX',
    NPI_ID: 'ID',
    POSTAL_CD: 'CODE',
    POSTAL_TYPE_CD: 'CODE',
    PREFIX: 'PREFIX',
    PRESCRIBER_IND: 'IND',
    PRIMARY_SPECIALTY_CD: 'CODE',
    PROVIDER_TYPE: 'TYPE',
    REC_SRC_ORIG_TS: '2010-10-10',
    SCREENING_STATUS: 'STATUS',
    STATE_CD: 'CODE',
    TITLE: 'TITLE',
    WORK_PHONE_NBR: 'NBR'
  };
};



const generateRecordOpt = (ACCT_SRC_CD, ACCT_SRC_NBR, BRAND_CD, EXTERNAL_RESPONSE_ID) => {
  return {
    PK_PK_ACCT_SRC_CD: ACCT_SRC_CD,
    PK_PK_ACCT_SRC_NBR: ACCT_SRC_NBR,
    PK_PK_BRAND_CD: BRAND_CD,
    PK_EXTERNAL_RESPONSE_ID: EXTERNAL_RESPONSE_ID,
    OPT_CHANNEL: 'CHANNEL',
    OPT_SRC: `OPTSRC_${getUniqueId().substr(0, 10)}`,
    OPT_STATUS: 'STATUS',
    OPT_TS: '2010-10-10',
    OPT_TYPE_CD: `OPTCODE_${getUniqueId().substr(0, 5)}`,
    PROGRAM_CD: `PCODE_${getUniqueId().substr(0, 10)}`
  };
};

const generateRecordSurveyResponse = (ACCT_SRC_CD, ACCT_SRC_NBR, BRAND_CD, EXTERNAL_RESPONSE_ID) => {
  return {
    PK_PK_ACCT_SRC_CD: ACCT_SRC_CD,
    PK_PK_ACCT_SRC_NBR: ACCT_SRC_NBR,
    PK_PK_BRAND_CD: BRAND_CD,
    PK_EXTERNAL_RESPONSE_ID: EXTERNAL_RESPONSE_ID,
    ANSWER_CD: 'CODE',
    ANSWER_TEXT: 'TEXT',
    QUESTION_CD: `QC_${getUniqueId().substr(0, 10)}`,
    SURVEYRESPONSE_TS: '2010-10-10',
    SURVEY_CD: `SC_${getUniqueId().substr(0, 10)}`
  };
};


const generateRecordResponse = (ACCT_SRC_CD, ACCT_SRC_NBR, BRAND_CD) => {
  return {
    PK_ACCT_SRC_CD: ACCT_SRC_CD,
    PK_ACCT_SRC_NBR: ACCT_SRC_NBR,
    PK_BRAND_CD: BRAND_CD,
    EXTERNAL_RESPONSE_ID: `ERI_${getUniqueId().substr(0, 10)}`,
    RESPONSE_CD: `RCD_${getUniqueId().substr(0, 10)}`,
    RESPONSE_DEVICE: 'DEVICE',
    RESPONSE_TS: '2010-10-10',
    TACTIC_CD: 'CODE'
  };
};

const generateRecordResponseFile = (ACCT_SRC_CD, ACCT_SRC_NBR, BRAND_CD, EXTERNAL_RESPONSE_ID) => {
  return {
    PK_ACCT_SRC_CD: ACCT_SRC_CD,
    PK_ACCT_SRC_NBR: ACCT_SRC_NBR,
    PK_BRAND_CD: BRAND_CD,
    EXTERNAL_RESPONSE_ID: EXTERNAL_RESPONSE_ID,
    RESPONSE_CD: `RCD_${getUniqueId().substr(0, 10)}`,
    RESPONSE_DEVICE: 'DEVICE',
    RESPONSE_TS: '2010-10-10',
    TACTIC_CD: 'CODE'
  };
};

const generateRecordOptFile = (ACCT_SRC_CD, ACCT_SRC_NBR, BRAND_CD, EXTERNAL_RESPONSE_ID, PROGRAM_CD) => {
  return {
    PK_PK_ACCT_SRC_CD: ACCT_SRC_CD,
    PK_PK_ACCT_SRC_NBR: ACCT_SRC_NBR,
    PK_PK_BRAND_CD: BRAND_CD,
    PK_EXTERNAL_RESPONSE_ID: EXTERNAL_RESPONSE_ID,
    OPT_CHANNEL: 'CHANNEL',
    OPT_SRC: `OPTSRC_${getUniqueId().substr(0, 10)}`,
    OPT_STATUS: 'STATUS',
    OPT_TS: '2010-10-10',
    OPT_TYPE_CD: `OPTCODE_${getUniqueId().substr(0, 10)}`,
    PROGRAM_CD: PROGRAM_CD
  };
};

const generateRecordSurveyResponseFile = (ACCT_SRC_CD, ACCT_SRC_NBR, BRAND_CD, EXTERNAL_RESPONSE_ID, QUESTION_CD, SURVEY_CD) => {
  return {
    PK_PK_ACCT_SRC_CD: ACCT_SRC_CD,
    PK_PK_ACCT_SRC_NBR: ACCT_SRC_NBR,
    PK_PK_BRAND_CD: BRAND_CD,
    PK_EXTERNAL_RESPONSE_ID: EXTERNAL_RESPONSE_ID,
    ANSWER_CD: 'CODE',
    ANSWER_TEXT: 'TEXT',
    QUESTION_CD: QUESTION_CD,
    SURVEYRESPONSE_TS: '2010-10-10',
    SURVEY_CD: SURVEY_CD
  };
};
const getUniqueId = () => {
  return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
};



async function processLineByLine() {
  let array = [];
  const fileStream = fs.createReadStream('keys.txt');

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });

  for await (const line of rl) {
    array.push(line.split(','))
  }

 // console.log(array);
  return array;
}


(async () => {
  try {
    await generateMessages();
  } catch (e) {
    console.log(e);
  }
})();
