module.exports = {
  kafka_topic: `${process.env.KAFKA_TOPIC}`,
  kafka_server: `${process.env.KAFKA_SERVER}`
};
