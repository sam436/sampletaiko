import { check } from 'k6';
import {  Rate, Trend } from "k6/metrics";
import http from "k6/http";


let deleteErrorRate = new Rate("Delete Entity errors");
let deleteTrend = new Trend("Delete Entity", true);
let deleteResultErrorRate = new Rate("Delete Entity Result errors");


let params = {
    headers: {
        "Content-Type": "application/json",
    },
};

let endpointUrl = `${__ENV.ENDPOINT_URL}`;

let array = [];


var data = open("./keys.txt").split(/\r?\n/).map(function (e) {
    return e;
});

for (let string of data) {
    if (isValid(string)) {
        array.push(JSON.stringify(string).split(','));
    }
}

function isValid(str) {
    if(Object.values(str).includes("_")){
        return true;
    }else{
        return false;
    }
}




export default function () {
    //console.log(array)
    for (let value of array) {
        deleteOperation(value[0].replace(/['"]+/g, ''),value[2].replace(/['"]+/g, ''),value[1].replace(/['"]+/g, ''))
    }
}


function deleteOperation(ACCT_SRC_CD,BRAND_CD,ACCT_SRC_NBR) {
    var deleteMutation = "mutation{ deleteCONTACT_P8(input:{ ACCT_SRC_CD:\""+ACCT_SRC_CD+"\" BRAND_CD:\""+BRAND_CD+"\" ACCT_SRC_NBR:\""+ACCT_SRC_NBR+"\" })}";

    // Data for the POST request
    let deleteOrgData = JSON.stringify({ query: deleteMutation });
    console.log(deleteOrgData)
    let deleteRequests = {
        "Delete Entity": {
            method: "POST",
            url: endpointUrl,
            params: params,
            body: deleteOrgData,
        },
    };

    let deleteResponses = http.batch(deleteRequests);
    let deleteResp = deleteResponses["Delete Entity"];

    check(deleteResp, {
        "status is 200": (r) => r.status === 200,
    }) || deleteErrorRate.add(1);

    deleteTrend.add(deleteResp.timings.duration);

    check(deleteResp, {
        "resultCode is 200": (r) => JSON.parse(deleteResp.body).resultCode === 200,
    }) || deleteResultErrorRate.add(1);

}



