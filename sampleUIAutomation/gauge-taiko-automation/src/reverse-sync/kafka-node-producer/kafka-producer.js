const kafka = require('kafka-node');
const config = require('./kafka-config');
const faker = require('faker');
const bluebird = require('bluebird');
const fs = require('fs');
const _ = require('lodash');
const path = require('path');
const kafkaOutputFile = path.resolve('../../../reports/k6');
const RECORDS_COUNT = 100;

const generateMessages = async () => {
  return new Promise((resolve, reject) => {
    try {
      let totalMessages = 0;
      let errorMessages = 0;
      let successMessages = 0;
      let totalRecords = 0;
      const client = new kafka.KafkaClient({ kafkaHost: config.kafka_server });
      const producer = new kafka.Producer(client, { partitionerType: 2 });
      const kafkaProducerSend = bluebird.promisify(producer.send, {
        context: producer
      });
      const kafkaProducerClose = bluebird.promisify(producer.close, {
        context: producer
      });
      producer.on('ready', async () => {
        try {
          let keepGoing = true;
          while (keepGoing === true) {
            totalMessages = successMessages + errorMessages;
            if (totalMessages === Number(process.env.NUMBER_OF_MESSAGES)) {
              keepGoing = false;
              break;
            }
            try {
              await kafkaProducerSend(generatePayload());
              successMessages++;
              console.log(`[kafka-producer -> ${config.kafka_topic}]: broker update success`);
            } catch (e) {
              errorMessages++;
              console.log(`[kafka-producer -> ${config.kafka_topic}]: broker update failed`);
            }
          }
          await kafkaProducerClose();
          console.log(`[kafka-producer -> ${config.kafka_topic}]: automated message generation completed`);
          totalRecords = successMessages * RECORDS_COUNT;
          const stats = {
            totalMessages,
            totalRecords,
            successMessages,
            errorMessages
          };
          console.log(`[kafka-producer -> ${config.kafka_topic}]: total number of messages produced`, stats);
          resolve(stats);
        } catch (error) {
          console.log(`[kafka-producer -> ${config.kafka_topic}]: error occurred in producer`);
          reject(error);
        }
      });

      producer.on('error', (err) => {
        console.log(`[kafka-producer -> ${config.kafka_topic}]: connection errored`);
        throw err;
      });
    } catch (err) {
      console.log(err);
      reject(err);
    }
  });
};

const generatePayload = () => {
  let records = [];
  for (let i = 0; i < RECORDS_COUNT; i++) {
    records.push(generateRecord());
  }
  const message = {
    entityName: 'ProfileV5',
    records
  };
  return [
    {
      topic: config.kafka_topic,
      messages: JSON.stringify(message)
    }
  ];
};

const generateRecord = () => {
  return {
    BIRTH_DATE: `${randomDate()}`,
    CREATE_DATE: null,
    CLIENT_JOIN_DATE: `${randomDate()}`,
    CLIENT_LASTUPDATE_DATE: `${randomDate()}`,
    COUNTRY_CODE: `${faker.address.countryCode()}`,
    CREATE_SRC: 'HIST_MIGRTN',
    CURRENT_OPT_SOURCE: null,
    DCS_ACTIVE_IND: null,
    DCS_AGE_RANGE: null,
    DCS_CHANNEL_LANGUAGE: null,
    DCS_DISPLAY_NAME: null,
    DCS_ETHNICITY: null,
    DCS_USERNAME: null,
    DELETED_FLAG: null,
    DWELLING_CODE: null,
    FIRST_NAME: `${faker.name.firstName()}`,
    FROZEN_FLAG: null,
    GENDER: `M`,
    GLOBAL_OPT_GMT: null,
    GLOBAL_OPT_OUT: null,
    GLOBAL_OPT_SOURCE: 'WELOPTCAEN',
    HOUSEHOLD_ID: '2000003497358',
    INDIVIDUAL_ID: 1000003487782,
    LANGUAGE_CODE: 'EN',
    LAST_NAME: `${faker.name.lastName()}`,
    MIDDLE_INIT: null,
    NAME_PREFIX: `${faker.name.prefix()}`,
    NAME_SUFFIX: `${faker.name.suffix()}`,
    NCOA_IND: 'B',
    OBJECT_FLAG: null,
    PROFILE_ID: `PerfTest_${getUniqueId()}}`,
    PROFILE_ID_ENC: 'B2Kzg4YT5rGCO2cZHCYuwA==',
    REASON: `${process.env.GO_PIPELINE_COUNTER}`,
    REMOVAL_TYPE: null,
    SHORT_NAME: null,
    STATUS: 'A',
    STATUS_REASON_CODE: null,
    TITLE: `${faker.name.title()}`,
    UPDATE_DATE: `${randomDate()}`,
    UPDATE_SRC: `${faker.random.alphaNumeric(3)}`
  };
};

const getUniqueId = () => {
  return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
};

const appendLeadingZeroes = (n) => {
  if (n <= 9) {
    return '0' + n;
  }
  return n;
};

const randomDate = () => {
  const currentDate = new Date();
  return `${currentDate.getFullYear()}-${appendLeadingZeroes(currentDate.getMonth() + 1)}-${appendLeadingZeroes(
    currentDate.getDate()
  )} ${appendLeadingZeroes(currentDate.getHours())}:${appendLeadingZeroes(currentDate.getMinutes())}:${appendLeadingZeroes(
    currentDate.getSeconds()
  )}`;
};

doesFileExist = (filePath) => {
  try {
    // get information of the specified file path.
    fs.statSync(filePath);
    return true;
  } catch (error) {
    return false;
  }
};

(async () => {
  try {
    let data = await generateMessages();
    if (!_.isEmpty(data) && typeof data === 'object') {
      try {
        const reportPath = `${kafkaOutputFile}/kafka-producer-report.json`;
        if (doesFileExist(reportPath)) {
          console.log(`Deleting old file`);
          /* If the file exists delete the file from system */
          fs.unlinkSync(reportPath);
        }
        fs.writeFileSync(reportPath, JSON.stringify(data));
        console.log(`Successfully created the kafka report!`);
      } catch (e) {
        console.log(e);
      }
    }
  } catch (e) {
    console.log(e);
  }
})();
