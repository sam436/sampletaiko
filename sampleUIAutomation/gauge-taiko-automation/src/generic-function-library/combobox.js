"use strict";
const { click, $, press } = require("taiko");

async function selectByFirstIndex(labelName) {
    await click($("//*[@name = '" + labelName + "']"));
    await press("Enter");    
}

async function getSelectedItem(labelName) {
    var itemValue = await $("//*[@name = '" + labelName + "']").text();
    return itemValue;
}

module.exports = {selectByFirstIndex, getSelectedItem};