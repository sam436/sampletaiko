"use strict";
const CryptoJS = require("crypto-js");

const Authentication = {
  getPassword: (ciphertext) => {
    const bytes  = CryptoJS.AES.decrypt(ciphertext, process.env.passkey);
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
  },
  setPassword: (password) => {
    const ciphertext = CryptoJS.AES.encrypt(password, process.env.passkey).toString();
    console.log(`Password Set!`, ciphertext);
  }
};

module.exports = Authentication;