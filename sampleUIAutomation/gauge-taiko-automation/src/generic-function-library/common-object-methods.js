"use strict";
const { click, $, text } = require("taiko");
const assert = require("assert");

async function verifyText(message) {
    assert.strictEqual(await text(message).exists(), true);
}

async function clickPrecedingSibling(msg) {
    await click($("//*[text() = '" + msg + "']//preceding-sibling::*"));
}

async function clickFollowingSibling(txt, elementToBeClicked) {
    await click($("//*[text() = '" + txt + "']//following-sibling::span[.='" + elementToBeClicked + "']"));
}

async function verifyAlertMessage(message, timeCount){      
    var timer = timeCount;      
    var obj = $("//*[@class = 'toast-message ng-star-inserted']");
    while (timer > 0) {                    
        if (await obj.exists() == true)
        {          
            var act = await obj.text(); 
            if (act == message)   
            {                          
                gauge.message(act + " is displayed as expected");     
                await click("×");                                     
                break;   
            }    
            else {
                gauge.message(message + " is not displayed as expected");
                gauge.message("Actual - " + await obj.text() + " / Expected - " + message);     
                await verifyText(message);                 
                await click("×");        
                break;
            }
                       
        }
        timer--;                             
    }        
    if (timer == 0) {
        gauge.message(message + " is not displayed as expected in specified time");
        await verifyText(message);           
    }
}

module.exports = {verifyText, clickPrecedingSibling, verifyAlertMessage, clickFollowingSibling};