/* globals gauge*/
"use strict";
const { write, click, into, evaluate, textField, $, waitFor, text } = require("taiko");
const { milliSeconds } = require("./utils");

async function getRowCount() {
        var count = 0;       
        if (await text("No Data Available").exists() == true) {    
                gauge.message("No Data Available");            
                return count;
        }       
        while (await $("//*[@class = 'table']//tbody/tr[" + count + "+ 1]").exists()) {                       
                count++;                                   
        }
        return count;
}

async function getEntireTableInAscending(col) {
        var arr = [];
        var rowCount = await getRowCount();    
        for (var i = 1; i <= rowCount; i++)  {   
                var actData = await $("//*[@class = 'table']//tbody/tr[" + i + "]/td[" + col + "]").text();   
                arr.push(actData);
        }         
        //return arr.sort();
        return arr.sort(Intl.Collator().compare);
}

async function getEntireTableInDescending(col) {
        var arr = [];
        var rowCount = await getRowCount();    
        for (var i = 1; i <= rowCount; i++)  {   
                var actData = await $("//*[@class = 'table']//tbody/tr[" + i + "]/td[" + col + "]").text();   
                arr.push(actData);
        } 
        //return arr.sort().reverse();     
        return arr.sort(Intl.Collator().compare).reverse();   
}

async function getEntireTable(col) {
        var arr = [];
        var rowCount = await getRowCount();    
        for (var i = 1; i <= rowCount; i++)  {   
                var actData = await $("//*[@class = 'table']//tbody/tr[" + i + "]/td[" + col + "]").text();   
                arr.push(actData);
        }         
        return arr;
}

async function selectRows(rows) {
        await click($("//*[@class='select-rows-per-page']"));
        await click(rows + " rows");
        await waitFor(milliSeconds(5));
}


async function checkPaginationButtonStatus () {
        var json = await evaluate($("//*[@class='pagination-button'][1]"), (element) => element.disabled);
        return json["result"];
}


async function verifyWebTable(data) {        
        var blnStatus = false;
        var val = data.split(",")        
        var rowCount = await getRowCount();                
        for (var i = 1; i <= rowCount; i++)  {                 
                for (var j = 0; j < val.length; j++) {                        
                        var col = val[j].split("=");
                        var actData = await $("//*[@class = 'table']//tbody/tr[" + i + "]/td[" + col[0] + "]").text();                        
                        if (actData == col[1]) {                                
                                for (var k = j + 1; k < val.length; k++) {                                         
                                        var col1 = val[k].split("=");
                                        var actData1 = await $("//*[@class = 'table']//tbody/tr[" + i + "]/td[" + col1[0] + "]").text();                                        
                                        if (actData1 == col1[1]) {
                                                blnStatus = true;                                                 
                                        } else {
                                                gauge.message("Expected Data:" + col1[1] + " / Actual Data:" + actData1);
                                                blnStatus = false;
                                                return blnStatus;                                                
                                        }
                                }
                        } else {
                                break;
                        }     
                        if (blnStatus == true) {
                                return blnStatus;                                
                        }                                   
                }
                if (i == rowCount) {
                        return blnStatus;                        
                }             
        } 
        return blnStatus;
}


async function arraysCompare(arr1, arr2) {
        var blnStatus = false;
        var i = arr1.length;        
        if (i != arr2.length) {
                return blnStatus;
        } 
        while (i--) {                
                if (arr1[i].toString().trim() === arr2[i].toString().trim()) {
                        blnStatus = true;
                } else {
                        gauge.message(arr1[i] + " / " + arr2[i]);                        
                        blnStatus = false;
                        break;                       
                }       
        }
        return blnStatus;
}

async function enterTableSerach(data) {
        await write(data, into(textField({ id: "tableSearch" })));   
        await click($('//*[@class="fas fa-search"]'));
        await waitFor(milliSeconds(3));
}

async function searchCompare(arr, searchText) {
        var blnStatus = false;
        var i = arr.length;
        
        while (i--) {                
                if (arr[i].toString().trim().toLowerCase().includes(searchText)) {
                        blnStatus = true;
                } else {
                        gauge.message(arr[i] + " / " + searchText);                        
                        blnStatus = false;
                        break;                       
                }       
        }
        return blnStatus;
}


module.exports = {getRowCount, verifyWebTable, getEntireTableInAscending, getEntireTableInDescending, getEntireTable, arraysCompare, selectRows, checkPaginationButtonStatus, enterTableSerach, searchCompare};