"use strict";
const { click } = require("taiko");

var objectMethods = {
    
    ClickEvent: function(text) {
        try{    
            click(text, { waitForNavigation: false });
        } catch (e) {
            console.error(e);
        }
    }    
};

module.exports = objectMethods;