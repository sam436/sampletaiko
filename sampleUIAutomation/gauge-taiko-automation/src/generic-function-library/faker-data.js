"use strict";
var faker = require("faker");

var fakerDataMethods = {  
        
    putKey: function (keyName) {   
        var definition = faker.fake("{{name.firstName}}");    
        gauge.dataStore.scenarioStore.put(keyName, definition);    
    },
    
    getKey: function (keyName) {      
        return gauge.dataStore.scenarioStore.get(keyName);    
    }    

};

module.exports = fakerDataMethods;