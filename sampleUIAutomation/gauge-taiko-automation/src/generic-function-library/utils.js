"use strict";

const utils = {  
    milliSeconds: (timeInSeconds) => {      
        return timeInSeconds * 1000;    
    }    
};

module.exports = utils;