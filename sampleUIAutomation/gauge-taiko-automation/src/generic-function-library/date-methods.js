"use strict";

var dateTimeMethods = {
    
    getTodaysDateTime: function() {
        try{    
            var today = new Date();
            var currentTime = today.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            return (today.getMonth() + 1 + "/" + today.getDate() + "/" + today.getFullYear() + ", " + currentTime);
        } catch (e) {
            console.error(e);
        }
    },

    getPastDateTime: function (number) {
        try {    
            var today = new Date();
            var pastDate = today.getDate() - number;
            var currentTime = today.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            return (today.getMonth() + 1 + "/" + pastDate + "/" + today.getFullYear() + ", " + currentTime);
        } catch (e) {
            console.error(e);
        }
    },

    addAccount: function (x, y, z) {
        return x + y + z;
    }
};

module.exports = dateTimeMethods;