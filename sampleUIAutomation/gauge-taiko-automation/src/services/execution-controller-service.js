"use strict";

const executionControllerSerivce = {
  recordsLookupStopExecution: false,
  specificationColumnDisplayed: false,
  getRecordsLookupStopExecution: () => {
    return this.recordsLookupStopExecution;
  },
  setRecordsLookupStopExecution: (isStopExecution) => {
    this.recordsLookupStopExecution = isStopExecution;
  },
  getSpecificationColumnDisplayed: () => {
    return this.specificationColumnDisplayed;
  },
  setSpecificationColumnDisplayed: (isSpecColumnDisplayed) => {
    this.specificationColumnDisplayed = isSpecColumnDisplayed;
  }
};

module.exports = executionControllerSerivce;
