    
const { write, click, into, textField, $ } = require('taiko');

var methods = {
    
    createNewAttributionJob: async function(jobname, AttributionStrategy, entity, brand, channel, fromtime, totime, drivingevent, lookback, weight) {   
        try {    
            //await click("Create New Attribution Job");
            await click("Processes");
            await click("Create Process");
            await click("Attribution Job");
            await write(jobname, into(textField({ placeholder: "Job_Name" })));
            await click("Select Attribution Strategy");
            await click(AttributionStrategy);
            await click("Select Entity Level");
            await click(entity);   
            await click("Select Brand(s)");
            await click($("//*[@id = 'item-2']"));
            await click(brand); 
            await click("Select Channel(s)");
            await click($("//*[@id = 'item-1']"));
            await click(channel);
            await click($("//*[@name = 'attributionJob.sequenceStart']"));
            await write(fromtime);
            await click($("//*[@name = 'attributionJob.sequenceEnd']"));
            await write(totime);    
            await click("Select an event");
            await click(drivingevent);
            await click($("//*[@placeholder = '#']"));
            await write(lookback)
            await click($("//*[@placeholder = '%']"));
            await write(weight);   
        } catch (e) {
            console.error(e)
        }    
    }
}

module.exports = methods;