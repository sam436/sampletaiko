
  "use strict";
  const { click } = require("taiko");
  
  async function createExternalTable(s3Location,fileName,database,databaseSchema) {
    //await write(tableName, into(textBox({ placeholder: "External_Table_Name" })));
    await click("Select an S3 Location");
    await click(s3Location);
    await click("Select a File Name");
    await click(fileName);
    await click("Select a Database");
    await click(database);
    await click("Select a Database Schema");
    await click(databaseSchema);
   // await click("Save");
  }
  
  module.exports = {createExternalTable};