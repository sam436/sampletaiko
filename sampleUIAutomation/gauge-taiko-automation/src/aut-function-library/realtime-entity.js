
  "use strict";
  const { write, click, into, textField, $ } = require("taiko");
  const objMethods = require("../generic-function-library/common-object-methods");
  
  async function createRealTimeEntity(entityName, description, definition, alert) {    
    await click("Create Data Set");
    await click("Real Time Entity");
    await write(entityName, into(textField({placeholder: "Real-Time-Entity-Name"})));
    await write(description, into($('//*[@placeholder="Enter Schema Description"]')));
    await write(definition, into($('//*[@placeholder="Enter Schema Definition"]')));
    await click("Save");
    objMethods.verifyAlertMessage(alert, 30);    
  }
  
  module.exports = {createRealTimeEntity};