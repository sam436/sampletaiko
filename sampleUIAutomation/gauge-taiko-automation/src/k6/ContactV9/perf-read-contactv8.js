import http from "k6/http";
import { check } from "k6";
import { Rate, Trend } from "k6/metrics";

let createErrorRate = new Rate("Read Entity errors");
let CreateTrend = new Trend("Read Entity");
let createResultErrorRate = new Rate("Read Entity Result errors");

export let options = {
  vus: `${__ENV.VUS}`,
  duration: `${__ENV.DURATION}`,
  thresholds: {
    "Read Entity": [__ENV.P_95 ? `"p(95)<${__ENV.P_95}"` : "p(95)<500"],
    "Read Entity": [__ENV.AVERAGE ? `"avg<${__ENV.AVERAGE}"` : "avg<300"],
  },
};

export default function () {
  let endpointUrl = `${__ENV.ENDPOINT_URL}`;
  let cycleValue = `${__ENV.CYCLE_VALUE}`;
  let min = `${__ENV.MIN}`;
  let max = `${__ENV.MAX}`;
  let randomProfileID = randomNumberGenerator(min, max);
  let randomBrandCD = randomNumberGenerator(1, 5);
  const mutation = `{CONTACT_V9s(filter: {BRAND_CD: "BRAND${randomBrandCD}", ACCT_SRC_CD: "SRC_CD_${cycleValue}", ACCT_SRC_NBR: "TEST_MONITOR_${cycleValue}_${randomProfileID}"}) {
        id
        BRAND_CD
        ACCT_SRC_CD
        ACCT_SRC_NBR
        FIRST_NM
        LAST_NM
        GENDER_CD
        EMAIL_ADDR
        HOME_PHONE_NBR
        ADDR_LINE_1
        ADDR_LINE_2
        CITY_NM
        STATE_CD
        POSTAL_CD
        RESPONSE {
          EXTERNAL_RESPONSE_ID
          RESPONSE_CD
          RESPONSE_TS
          OPT {
            OPT_CHANNEL
            OPT_STATUS
            PROGRAM_CD
            OPT_TS
          }
        }
      }
    }`;
  let params = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  // Data for the POST request
  let createOrgData = JSON.stringify({ query: mutation });
  let requests = {
    "Read Entity": {
      method: "POST",
      url: endpointUrl,
      params: params,
      body: createOrgData,
    },
  };

  let responses = http.batch(requests);
  let createResp = responses["Read Entity"];

  check(createResp, {
    "status is 200": (r) => r.status === 200,
  }) || createErrorRate.add(1);

  CreateTrend.add(createResp.timings.duration);

  check(createResp, {
    "resultCode is 200": (r) => JSON.parse(createResp.body).resultCode === 200,
  }) || createResultErrorRate.add(1);
}

function randomNumberGenerator(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
