const minimist = require("minimist");
const fs = require("fs");
const path = require("path");
const opts = minimist(process.argv.slice(2));
let buildPathHtml = path.resolve("../../../reports/k6");

class CreateK6Report {
  constructor() {}

  doesFileExist(filePath){
    try {
      // get information of the specified file path.
      fs.statSync(filePath);
      return true;
    } catch (error) {
      return false;
    }
  };

  convertSize(bytes){
    if (isNaN(bytes) && !bytes) {
      return;
    }
    let i = Math.floor(Math.log(bytes) / Math.log(1024)),
      sizes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    return (bytes / Math.pow(1024, i)).toFixed(2) * 1 + "" + sizes[i];
  };

  convertTime(millisec){
    if (isNaN(millisec) && !millisec) {
      return;
    }
    let ms = millisec.toFixed(2);
    let seconds = (millisec / 1000).toFixed(2);
    let minutes = (millisec / (1000 * 60)).toFixed(2);
    let hours = (millisec / (1000 * 60 * 60)).toFixed(2);
    let days = (millisec / (1000 * 60 * 60 * 24)).toFixed(2);
    if (millisec < 1000) {
      return ms + "ms";
    } else if (seconds < 60) {
      return seconds + "sec";
    } else if (minutes < 60) {
      return minutes + "min";
    } else if (hours < 24) {
      return hours + "hrs";
    } else {
      return days + "days";
    }
  };

  createStats(statsData, checksData){
    if (!statsData && !checksData) {
      return;
    }
    return `
          <table class="ui definition compact table">
            <tbody>
              ${this.populateRows(checksData)}
              ${this.populateRows(statsData)}
            </tbody>
          </table>
        `;
  };

  populateRows(row){
    let rowValue = ``;
    if (!row) {
      return;
    }
    for (let key in row) {
      rowValue += `
          <tr>
            <td><code>${key}</code></td>
            ${this.populateRowData(row[key], key)}
          </tr>`;
    }
    return rowValue;
  };

  populateRowData(rowData, parentKey){
    let keys = ["avg", "max", "med", "min", "p(90)", "p(95)"];
    let ignoreKeys = ["name", "path", "id", "thresholds"];
    let output = ``;
    if (!rowData) {
      return;
    }
    for (let key in rowData) {
      if (
        keys.includes(key) &&
        parentKey !== "vus" &&
        parentKey !== "vus_max"
      ) {
        output += `${key}=<span>${this.convertTime(
          Number(rowData[key])
        )}</span> `;
      } else if (key === "rate") {
        if (parentKey === "data_sent" || parentKey === "data_received") {
          output += `${key}=<span>${this.convertSize(
            Number(rowData[key])
          )}/s</span> `;
        } else {
          output += `${key}=<span>${Number(rowData[key]).toFixed(2)}/s</span> `;
        }
      } else if (key === "count") {
        if (parentKey === "data_sent" || parentKey === "data_received") {
          output += `${key}=<span>${this.convertSize(
            Number(rowData[key])
          )}</span> `;
        } else {
          output += `${key}=<span>${this.removeEscapeChar(
            rowData[key]
          )}</span> `;
        }
      } else if (key === "passes") {
        output += `<i class="check icon" style="color:green;"></i>${this.removeEscapeChar(
          rowData[key]
        )} `;
      } else if (key === "fails") {
        output += `<i class="x icon" style="color:red;"></i>${this.removeEscapeChar(
          rowData[key]
        )} `;
      } else if (ignoreKeys.includes(key)) {
        output += ``;
      } else {
        output += `${key}=<span>${this.removeEscapeChar(rowData[key])}</span> `;
      }
    }
    return `<td><code class="td-value">${output}</code></td>`;
  };

  removeEscapeChar(val){
    return JSON.stringify(val).replace(/\\/g, "");
  };

  createHtml(stats) {
    return `
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>K6 Report</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.9/semantic.min.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.9/semantic.min.js"></script>
        <style>
          .container {
            margin-top:10px;
          }    
          span {
             color: blue;
          }      
          .td-value {
            background-color: rgba(0, 0, 0, 0.08);
            border-radius: 3px;
            display: inline-block;
            font-weight: bold;
            padding: 1px 6px;
            vertical-align: baseline;
          }
        </style>
    </head>
    <body>
    <div class="ui container">
       <div class="ui raised inverted segment">   
          <h2 class="ui horizontal divider header">
             <i class="bar chart icon"></i>
             Performance Statistics
          </h2>
            ${stats}
        </div>
    </div>

    </body>
  </html>
  `;
  }

  async generateReport(){
    if (!opts.file) {
      console.error(
        `Please include the JSON file using '--file=' parameter, Eg: --file=test.json`
      );
      process.exit(1);
    } else {
      try {
        this.jsonPath = `${opts.file}.json`;
        console.log(`LOG: Input File path  -> `,  this.jsonPath);
        let jsonData = JSON.parse(fs.readFileSync(this.jsonPath, "utf-8"));
        buildPathHtml = `${buildPathHtml}/${opts.file}.html`
        console.log(`LOG: Output Report file path  -> `, buildPathHtml);
        /* Check if the file for `html` build exists in system or not */
        if (this.doesFileExist(buildPathHtml)) {
          console.log(`Deleting old build file`);
          /* If the file exists delete the file from system */
          fs.unlinkSync(buildPathHtml);
        }
        const stats = this.createStats(
          jsonData.metrics,
          jsonData.root_group.checks
        );
        /* generate html */
        const html = this.createHtml(stats);
        /* write the generated html to file */
        fs.writeFileSync(buildPathHtml, html);
        console.log(`Successfully created the report!`);
      } catch (error) {
        console.error(`Error generating the report, ${error}`);
      }
    }
  };
}

(async () => {
  const report1 = new CreateK6Report();
  await report1.generateReport();
})();
