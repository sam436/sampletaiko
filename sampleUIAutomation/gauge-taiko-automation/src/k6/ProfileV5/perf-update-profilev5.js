import http from "k6/http";
import {check} from "k6";
import {Trend, Rate} from "k6/metrics";
import faker from "cdnjs.com/libraries/Faker";

let createErrorRate = new Rate("Update Entity errors");
let CreateTrend = new Trend("Update Entity");
let CreateResultTrend = new Trend("Update Entity Result");
let createResultErrorRate = new Rate("Update Entity Result errors");


export let options = {
    stages: [
        // {target: 1, duration: '30s'},
        // {target: 10, duration: '30s'},
        // {target: 25, duration: '30s'},
        {target: 50, duration: '720m'},

    ],
    thresholds: {
        "Update Entity": [(__ENV.P_95 ? `"p(95)<${__ENV.P_95}"` : "p(95)<1500")],
        "Update Entity": [(__ENV.AVERAGE ? `"avg<${__ENV.AVERAGE}"` : "avg<1000")],
    }
};

export default function () {
    let endpointUrl = `${__ENV.ENDPOINT_URL}`;
    let cycleValue = `${__ENV.CYCLE_VALUE}`;
    let min = `${__ENV.MIN}`;
    let max = `${__ENV.MAX}`;
    const randomCapital = () => String.fromCharCode(65 + Math.floor(Math.random() * 26));
    const randomGender = faker.random.arrayElement(["M", "F"]);
    const randomBool = faker.random.arrayElement(["Y", "N"]);
    let iterOffset = __ENV.OFFSET ? Number(__ENV.OFFSET) : 0;
    let newval = iterOffset + __ITER;
    let randomProfileID = `ProfileId_${cycleValue}_${__VU}${newval}`;

    const mutation = `
    mutation {
  updateProfileV5(input: {BirthDt: "", ClientJoinDt: "${randomDate()}", ClientLastupdtDt: "${randomDate()}", CntryCd: "${faker.address.countryCode()}", CreateSrc: "${faker.lorem.word()}", CurrentOptSrc: "", DcsActiveInd: "", DcsAgeRange: "", DcsChLang: "", DcsDisplayNm: "", DcsEthnicity: "", DcsUsernm: "", DeldFlg: "", DwgCd: "", FirstNm: "${faker.name.firstName()}", FrozenFlg: "", Gender: "${randomGender}", GlobalOptGmt: "", GlobalOptOut: "", GlobalOptSrc: "${getUniqueId()}", HouseholdId: "${getUniqueId()}", IndivId: ${faker.finance.amount(0, 10000000, 4)}, LangCd: "EN", LastNm: "${faker.name.lastName()}", MiddleInit: "", NmPrefix: "", NmSuffix: "", NcoaInd: "${randomCapital()}", ObjectFlg: "", ProfileId: "${randomProfileID}", ProfileIdEnc: "${getUniqueId()}", Reason: "", RemovalType: "", ShortNm: "Doug", Status: "${randomCapital()}", StatusReasonCd: "", Title: "", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}", ProfileAddr: [{AddrId: "${getUniqueId()}", AddrLine1: "", AddrLine2: "", AddrLine3: "", AddrLine4: "", AddrType: "", BusResInd: "", CarrierRoute: "", ChCd: "", City: "", CompanyNm: "", CntryCd: "${faker.address.countryCode()}", CntyCd: "", CreateSrc: "${faker.lorem.word()}", DcsActiveInd: "", DcsAddrLocale: "", DcsAddrPrefTypeId: "", DcsAllowPromotions: "", DcsArea: "", DcsBlock: "", DcsBuilding: "", DcsCntyCd: "", DcsDepartment: "", DcsDistrict: "", DcsGarden: "", DcsHouseNum: "", DcsIsAmberZone: "", DcsIsRedZone: "", DcsLandMark: "", DcsLatitude: "", DcsLvl: "", DcsLongtiude: "", DcsPrimaryInd: "", DcsRemark: "", DcsShortZip: "", DcsStreetLonNum: "", DcsStreetType: "", DcsSuburb: "", DcsUnit: "", DelvModeType: "", FinalAddrSrc: "", GlobalAddrId: "${getUniqueId()}", Latitude: ${faker.address.latitude()}, Loc: "S", Longitude: ${faker.address.longitude()}, MailScore: "", MultiTypeCd: "${randomCapital()}", NcoaLinkFootnote: "", NcoaMoveDt: "", OccupancyScore: "", PoCd: "${faker.random.alphaNumeric(5)}", PreferredInd: "${randomBool}", StateCd: "", Status: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", Urbanization: "", UspsCity: "", UspsStateCd: "", VacantInd: "", CreateDt: "${randomDate()}"}], ProfileAttr: [{AttrCd: "${faker.random.alphaNumeric(4)}", AttrVal: "${faker.random.alphaNumeric(4)}", CreateSrc: "${faker.lorem.word()}", Status: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}, {AttrCd: "${faker.random.alphaNumeric(4)}", AttrVal: "${faker.random.alphaNumeric(3)}", CreateSrc: "${faker.lorem.word()}", Status: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}, {AttrCd: "${faker.random.alphaNumeric(4)}", AttrVal: "${faker.random.alphaNumeric(3)}", CreateSrc: "${faker.lorem.word()}", Status: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}, {AttrCd: "${faker.random.alphaNumeric(4)}", AttrVal: "${faker.random.alphaNumeric(3)}", CreateSrc: "${faker.lorem.word()}", Status: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}, {AttrCd: "${faker.random.alphaNumeric(4)}", AttrVal: "${faker.random.alphaNumeric(3)}", CreateSrc: "${faker.lorem.word()}", Status: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}], ProfileEmail: [{ChCd: "${faker.random.alphaNumeric(2)}", CreateSrc: "${faker.lorem.word()}", DcsActiveInd: "", DcsCreatedAt: "", DcsCreatedBy: "", DcsLastmodAt: "", DcsLastmodBy: "", DcsPrimaryInd: "", DcsVerifiedDt: "", DcsVerifiedInd: "", DelvStatus: "${randomBool}", EmailAddr: "${faker.internet.email()}", EmailAddrEnc: "${faker.random.alphaNumeric(10)}", EmailId: "${faker.internet.userName()}", Loc: "", PreferredInd: "${randomBool}", SpamStatus: "", Status: "${randomCapital()}", UniqueEmailId: ${faker.finance.amount(0, 10000000, 4)}, UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}], ProfileEnroll: [{AcceptedTcFlg: "${randomBool}", CreateSrc: "${faker.lorem.word()}", EnrollId: "${getUniqueId()}", PrivacyPolicyAcceptDt: "${randomDate()}", ProgCd: "${faker.random.alphaNumeric(6)}", RefEnrollChCd: "${faker.random.alphaNumeric(3)}", StartDt: "${randomDate()}", Status: "${randomCapital()}", TcAcceptDt: "${randomDate()}", ProfileEnrollRest: [{Comments: "${faker.lorem.words()}", CreateSrc: "${faker.lorem.word()}", DtLocPref: "${randomDate()}", DcsFavUid: "", PurchaseLocPref: "${randomBool}", RestaurantId: "${getUniqueId()}", RestaurantInteractionCd: "${faker.random.alphaNumeric(3)}", RestaurantNm: "${faker.name.firstName()}", RestaurantNum: "${faker.random.number(1000000)}", Status: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}], UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}], ProfileExtKeys: [{CreateSrc: "${faker.lorem.word()}", DcsActiveInd: "", DcsAppId: "", DcsAppNm: "", DcsCreatedAt: "", DcsCreatedBy: "", DcsLastmodAt: "", DcsLastmodBy: "", ExtKey1: "20001", ExtKey2: "", ExtKey3: "", ProfileExtKeysId: "${getUniqueId()}", RegSrcCd: "${faker.random.alphaNumeric(4)}", Status: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}], ProfilePhone: [{ChCd: "${faker.random.alphaNumeric(2)}", CntryCd: "${faker.address.countryCode()}", CreateSrc: "${faker.lorem.word()}", DcsActiveInd: "", DcsCreatedAt: "", DcsCreatedBy: "", DcsLastmodAt: "", DcsLastmodBy: "", DcsPrimaryInd: "", DcsVerifiedDt: "", DcsVerifiedInd: "", DelvStatus: "${randomBool}", Frequency: ${faker.finance.amount(0, 10000, 0)}, Loc: "${randomCapital()}", PhoneId: "${faker.random.alphaNumeric(16)}", PhoneNum: "${faker.phone.phoneNumber()}", PhoneType: "${faker.random.alphaNumeric(3)}", PreferredInd: "${randomBool}", Status: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.lorem.word()}", CreateDt: "${randomDate()}"}], ProfileSubscr: [{AddrLine1: "", AddrLine2: "", AddrLine3: "", AddrLine4: "", ChCd: "${faker.random.alphaNumeric(2)}", City: "", CntryCd: "", CreateSrc: "prefctrinitreg", DcsCreatedAt: "", DcsCreatedBy: "", DcsLastmodAt: "", DcsLastmodBy: "", DcsOptOutReason: "", EmailAddr: "dougb@yahoo.com", OptDt: "${randomDate()}", OptSrc: "", OptStatus: "${randomBool}", PhoneNum: "", PoCd: "", ProgCd: "", RegSrcCd: "${faker.random.alphaNumeric(8)}", SocialUid: "", StateCd: "", Status: "A", SubscrId: "${faker.random.alphaNumeric(8)}", UpdtDt: "${randomDate()}", UpdtSrcCd: "${faker.random.alphaNumeric(8)}", UpdtSrc: "${faker.random.alphaNumeric(3)}", UpdtdByMethod: "", CreateDt: ""}, {AddrLine1: "", AddrLine2: "", AddrLine3: "", AddrLine4: "", ChCd: "${faker.random.alphaNumeric(2)}", City: "", CntryCd: "", CreateSrc: "${faker.lorem.word()}", DcsCreatedAt: "", DcsCreatedBy: "", DcsLastmodAt: "", DcsLastmodBy: "", DcsOptOutReason: "", EmailAddr: "${faker.internet.email()}", OptDt: "${randomDate()}", OptSrc: "", OptStatus: "${randomBool}", PhoneNum: "", PoCd: "", ProgCd: "", RegSrcCd: "MPPZCA185", SocialUid: "", StateCd: "", Status: "A", SubscrId: "${faker.random.alphaNumeric(8)}", UpdtDt: "${randomDate()}", UpdtSrcCd: "${faker.random.alphaNumeric(8)}", UpdtSrc: "${faker.lorem.word()}", UpdtdByMethod: "", CreateDt: "${randomDate()}"}], ProfileDcsAcceptPolicy: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}], ProfileDcsAccessPolicy: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}], ProfileCcpCim: [{CimId: "${getUniqueId()}", CreateDt: "${randomDate()}", CreateSrc: "${faker.random.alphaNumeric(4)}", RetryCnt: ${faker.finance.amount(0, 10000000, 4)}, Sentstatus: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.random.alphaNumeric(4)}"}, {CimId: "${getUniqueId()}", CreateDt: "${randomDate()}", CreateSrc: "${faker.random.alphaNumeric(4)}", RetryCnt: ${faker.finance.amount(0, 10000000, 4)}, Sentstatus: "${randomCapital()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.random.alphaNumeric(4)}"}], ProfileDcsAudit: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}], ProfileDcsPrefs: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}], ProfileDcsFavs: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}], ProfileDcsDevices: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}", DcsDeviceId: "${faker.random.alphaNumeric(9)}", DcsDeviceIdType: "${faker.random.alphaNumeric(3)}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}", DcsDeviceId: "${faker.random.alphaNumeric(9)}", DcsDeviceIdType: "${faker.random.alphaNumeric(3)}"}], ProfileSurvey: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}"}], SocProfile: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}", SocialUid: "${faker.random.uuid()}", UpdtDt:"${randomDate()}", UpdtSrc: "${faker.random.alphaNumeric(4)}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}", SocialUid: "${faker.random.uuid()}", UpdtDt:"${randomDate()}", UpdtSrc: "${faker.random.alphaNumeric(4)}"}], ProfileExtIdsMap: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}", DcsId: "${faker.random.alphaNumeric(4)}", CntryCd: "${faker.address.countryCode()}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}", DcsId: "${faker.random.alphaNumeric(4)}", CntryCd: "${faker.address.countryCode()}"}], ProfileSuppression: [{CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}", EmailAddr: "${faker.internet.email()}", UniqueEmailId: "${faker.internet.userName()}", Status: "${randomCapital()}",SuppressFlg: "${randomBool}", CntryCd: "${faker.address.countryCode()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.random.alphaNumeric(3)}"}, {CreateDt: "${randomDate()}", CreateSrc: "${getUniqueId()}", EmailAddr: "${faker.internet.email()}", UniqueEmailId: "${faker.internet.userName()}", Status: "${randomCapital()}",SuppressFlg: "${randomBool}", CntryCd: "${faker.address.countryCode()}", UpdtDt: "${randomDate()}", UpdtSrc: "${faker.random.alphaNumeric(3)}"}], ProfileOptFields: [{AllEmailOptinEffDt: "${randomDate()}", AllEmailOptinLatestDt: "${randomDate()}", AllPushOptinEffDt: "${randomDate()}", AllPushOptinLatestDt: "${randomDate()}", AllSmsOptinEffDt: "${randomDate()}", AllSmsOptinLatestDt: "${randomDate()}", AllPushlOptinStatus: "${randomCapital()}", AllEmailOptinStatus: "${randomCapital()}"}, {AllEmailOptinEffDt: "${randomDate()}", AllEmailOptinLatestDt: "${randomDate()}", AllPushOptinEffDt: "${randomDate()}", AllPushOptinLatestDt: "${randomDate()}", AllSmsOptinEffDt: "${randomDate()}", AllSmsOptinLatestDt: "${randomDate()}", AllPushlOptinStatus: "${randomCapital()}", AllEmailOptinStatus: "${randomCapital()}"}]}) {
     id
    }
  }`;
    let params = {
        headers: {
            "Content-Type": "application/json"
        }
    };

    // Data for the POST request
    let createOrgData = JSON.stringify({query: mutation});
    let requests = {
        "Update Entity": {
            method: "POST",
            url: endpointUrl,
            params: params,
            body: createOrgData
        },
    };

    let responses = http.batch(requests);
    //console.log(JSON.stringify(responses));
    let createResp = responses["Update Entity"];

    check(createResp, {
        "status is 200": (r) => r.status === 200
    }) || createErrorRate.add(1);

    CreateTrend.add(createResp.timings.duration);

    check(createResp, {
        "resultCode is 200": (r) => JSON.parse(createResp.body).resultCode === 200
    }) || createResultErrorRate.add(1);
};

function getUniqueId() {
    return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
}

function randomNumberGenerator(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function appendLeadingZeroes(n) {
    if (n <= 9) {
        return "0" + n;
    }
    return n
}

function randomDate() {
    const currentDate = new Date();
    const now = currentDate.getFullYear() + "-" + appendLeadingZeroes(currentDate.getMonth() + 1) + "-" + appendLeadingZeroes(currentDate.getDate()) + " " + appendLeadingZeroes(currentDate.getHours()) + ":" + appendLeadingZeroes(currentDate.getMinutes()) + ":" + appendLeadingZeroes(currentDate.getSeconds());
    return now
}
