import http from "k6/http";
import {check} from "k6";
import {Trend, Rate} from "k6/metrics";
import faker from "https://cdnjs.cloudflare.com/ajax/libs/Faker/3.1.0/faker.min.js";

let createErrorRate = new Rate("Update Entity errors");
let CreateTrend = new Trend("Update Entity");
let createResultErrorRate = new Rate("Update Entity Result errors");


export let options = {
    vus: `${__ENV.VUS}`,
    duration: `${__ENV.DURATION}`,
    thresholds: {
        "Update Entity": [(__ENV.P_95 ? `"p(95)<${__ENV.P_95}"` : "p(95)<500")],
        "Update Entity": [(__ENV.AVERAGE ? `"avg<${__ENV.AVERAGE}"` : "avg<300")],
    }
};

export default function () {
    let endpointUrl = `${__ENV.ENDPOINT_URL}`;
    let cycleValue = `${__ENV.CYCLE_VALUE}`;
    let min = `${__ENV.MIN}`;
    let max = `${__ENV.MAX}`;
    let randomProfileID = randomNumberGenerator(min, max);
    let randomBrandCD = randomNumberGenerator(1, 5);
    const randomCapital = () => String.fromCharCode(65 + Math.floor(Math.random() * 26));
    const mutation = `mutation{updateCONTACT_V8(input: {BRAND_CD: "BRAND${randomBrandCD}", ACCT_SRC_CD: "SRC_CD_${cycleValue}", ACCT_SRC_NBR: "TEST_MONITOR_${cycleValue}_${randomProfileID}", CONTACT_TYPE_CD: "${randomCapital()}", EXCLUSION_CD: "${faker.lorem.word()}", EMAIL_ADDR: "${faker.internet.email()}", CONTACT_TS: "${randomDate()}", REC_SRC_ORIG_TS: "${randomDate()}", FIRST_NM: "${faker.name.firstName()}", LAST_NM: "${faker.name.lastName()}", ADDR_LINE_1: "${faker.address.streetName()}", CITY_NM: "${faker.address.city()}", STATE_CD: "${faker.address.state()}", RESPONSE: {EXTERNAL_RESPONSE_ID: "UNIQUEID-RESP${getUniqueId()}", RESPONSE_CD: "INGUCON_JUICE_RSP_${__ITER}", RESPONSE_TS: "${randomDate()}", OPT: [{OPT_SRC: "${faker.lorem.word()}", OPT_TS: "${randomDate()}", OPT_CHANNEL: "DM", PROGRAM_CD: "${faker.lorem.word()}", OPT_TYPE_CD: "${faker.lorem.word()}", OPT_STATUS: "${randomCapital()}"}, {OPT_SRC: "${faker.lorem.word()}", OPT_TS: "${randomDate()}", OPT_CHANNEL: "${faker.lorem.word()}", PROGRAM_CD: "${faker.lorem.word()}", OPT_TYPE_CD: "${faker.lorem.word()}", OPT_STATUS: "${randomCapital()}"}], SURVEYRESPONSE: [{SURVEYRESPONSE_TS: "${randomDate()}", QUESTION_CD: "${faker.lorem.word()}", ANSWER_CD: "${faker.lorem.word()}", ANSWER_TEXT: "${faker.lorem.word()}", SURVEY_CD: "${faker.lorem.word()}"}]}})
		{
		  id
		  BRAND_CD
		  ACCT_SRC_CD
		  ACCT_SRC_NBR
		  FIRST_NM
		  LAST_NM
		  GENDER_CD
		  EMAIL_ADDR
		  HOME_PHONE_NBR
		  ADDR_LINE_1
		  ADDR_LINE_2
		  CITY_NM
		  STATE_CD
		  POSTAL_CD
		  RESPONSE {
			EXTERNAL_RESPONSE_ID
			RESPONSE_CD
			RESPONSE_TS
			OPT {
			  OPT_CHANNEL
			  OPT_STATUS
			  PROGRAM_CD
			  OPT_TS
			}
		  }
		}
		}`
    let params = {
        headers: {
            "Content-Type": "application/json"
        }
    };

    // Data for the POST request
    let createOrgData = JSON.stringify({query: mutation});
    let requests = {
        "Update Entity": {
            method: "POST",
            url: endpointUrl,
            params: params,
            body: createOrgData
        },
    };

    let responses = http.batch(requests);
    let createResp = responses["Update Entity"];

    check(createResp, {
        "status is 200": (r) => r.status === 200
    }) || createErrorRate.add(1);

    CreateTrend.add(createResp.timings.duration);

    check(createResp, {
        "resultCode is 200": (r) => JSON.parse(createResp.body).resultCode === 200
    }) || createResultErrorRate.add(1);
};

function randomNumberGenerator(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function appendLeadingZeroes(n) {
    if (n <= 9) {
        return "0" + n;
    }
    return n
}

function getUniqueId() {
    return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
}

function randomDate() {
    const currentDate = new Date();
    const now = currentDate.getFullYear() + "-" + appendLeadingZeroes(currentDate.getMonth() + 1) + "-" + appendLeadingZeroes(currentDate.getDate()) + " " + appendLeadingZeroes(currentDate.getHours()) + ":" + appendLeadingZeroes(currentDate.getMinutes()) + ":" + appendLeadingZeroes(currentDate.getSeconds());
    return now
}
