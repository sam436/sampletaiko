import http from "k6/http";
import { check } from "k6";
import { Rate, Trend } from "k6/metrics";
import faker from "https://cdnjs.cloudflare.com/ajax/libs/Faker/3.1.0/faker.min.js";

let createErrorRate = new Rate("SC Mutation errors");
let CreateTrend = new Trend("SC Mutation");
let createResultErrorRate = new Rate("SC Mutation Result errors");

let randomBrandCD = randomNumberGenerator(1, 5);

export let options = {
    vus: `${__ENV.VUS}`,
    duration: `${__ENV.DURATION}`,
    thresholds: {
        "SC Mutation": [__ENV.P_95 ? `"p(95)<${__ENV.P_95}"` : "p(95)<500"],
        "SC Mutation": [__ENV.AVERAGE ? `"avg<${__ENV.AVERAGE}"` : "avg<300"],
    },
};


export default function () {
    let executeEndpointUrl = `${__ENV.ENDPOINT_URL}`;
    const executeSeeding = `mutation {updateSourceSystem_Keys_Metadata(input : {Marketing_Entity : "BRAND_${randomBrandCD}",Fields : "[ 'SrcEmailAddr1','StdEmailAddr2']",Data_Source :"DYYtestcd",Activity_ts: "${randomDate()}",Method : "deterministic", Trusted_Ind: "Y" }){Activity_ts Data_Source
        Marketing_Entity Fields Method Trusted_Ind}}`;
    let execParam = {
        headers: {
            "Content-Type": "application/json",
        },
    };

    // Data for the POST request
    let createExecuteOrgData = JSON.stringify({ query: executeSeeding });
    let executeRequests = {
        "SC Mutation": {
            method: "POST",
            url: executeEndpointUrl,
            params: execParam,
            body: createExecuteOrgData,
        },
    };

    let executeResponses = http.batch(executeRequests);
    let createExecResp = executeResponses["SC Mutation"];
	console.log(createExecResp.body);

    check(createExecResp, {
        "status is 200": (r) => r.status === 200,
    }) || createErrorRate.add(1);

    CreateTrend.add(createExecResp.timings.duration);

    check(createExecResp, {
        "resultCode is 200": (r) => JSON.parse(createExecResp.body).resultCode === 200,
    }) || createResultErrorRate.add(1);

}

function randomNumberGenerator(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function appendLeadingZeroes(n) {
    if (n <= 9) {
        return "0" + n;
    }
    return n;
}

function getUniqueId() {
    return (
        Date.now().toString(36) + Math.random().toString(36).substr(2, 5)
    ).toUpperCase();
}

function randomDate() {
    const currentDate = new Date();
    const now = currentDate.getFullYear() + "-" + appendLeadingZeroes(currentDate.getMonth() + 1) + "-" + appendLeadingZeroes(currentDate.getDate()) + " " + appendLeadingZeroes(currentDate.getHours()) + ":" + appendLeadingZeroes(currentDate.getMinutes()) + ":" + appendLeadingZeroes(currentDate.getSeconds());
    return now
}
function getRandomString() {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var result = '';
    for ( var i = 0; i < 3; i++ ) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}
function randomCodeGen() {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var result = '';
    for ( var i = 0; i < 2; i++ ) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}

