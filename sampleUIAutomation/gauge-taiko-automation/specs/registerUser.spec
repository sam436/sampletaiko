# Verification of new registration page
Tags : verifyRegistration

Date Created        : 06/09/2021
Version             : 1.0.0
Owner               : Sunaina Awadhiya
Description         : Verification of registration page

table: resources/data/registrationPage.csv

## Verify user is not directed to successfull registration message with empty password
* Open Browser
* Open main page
* Click on Login
* Click "Not a member yet?  Register for a free trial." link
* Register user with <ts2_emailAdress> <ts2_fName> <ts2_lName> <ts2_password> and verify message <ts2_succMessage>
* Close Browser

## Verify user is not directed to successfull registration message with wrong password
* Open Browser
* Open main page
* Click on Login
* Click "Not a member yet?  Register for a free trial." link
* Register user with <ts3_emailAdress> <ts3_fName> <ts3_lName> <ts3_password> and verify message <ts3_succMessage>
* Close Browser

## Verify user gets successfull registration message once request is submitted
* Open Browser
* Open main page
* Click on Login
* Click "Not a member yet?  Register for a free trial." link
* Register user with <ts1_emailAdress> <ts1_fName> <ts1_lName> <ts1_password> and verify message <ts1_succMessage>
* Close Browser
