# Edit configuration parameters of a package

Date Created        : 01/29/2020
Version             : 1.0.0
JIRA ID             : AUB-7034
Owner               : Prabish Prakash (Rhythm)
Modified By         : Prabish Prakash (Rhythm)
Description         : Edit configuration parameters

* Open Browser

## Edit configuration parameter and verify validation message displayed
* Go to Processes page and click settings
* Click Package Repository
* Enter package name "AWSLambdaSample3" and search
* Scroll to the package with name "AWSLambdaSample3" and version "0.0.0.22" click on more options link
* Click on Edit Configuration Parameters to launch the modal window
* Click link Add Another Parameter
* Enter invalid parameter name and verify if validator error is displayed

// ## Edit configuration parameter for a package and save and verify saved parameters
// * Go to Processes page and click settings
// * Click Package Repository
// * Enter package name "StringUperCaseLambda" and search
// * Select 100 rows from pagination dropdown
// * Scroll to the package with name "StringUperCaseLambda" and version "2.3.173.13" click on more options link
// * Click on Edit Configuration Parameters to launch the modal window
// * Click link Add Another Parameter
// * Enter the parameter name and parameter value and click on Save
// * Scroll to the package with name "StringUperCaseLambda" and version "2.3.173.13" click on more options link
// * Click on Edit Configuration Parameters to launch the modal window
// * Verify if added parameter name and parameter value exists  

// ## Edit configuration parameter verify cancel functionality
// * Go to Processes page and click settings
// * Click Package Repository
// * Enter package name "StringUperCaseLambda" and search
// * Select 100 rows from pagination dropdown
// * Scroll to the package with name "StringUperCaseLambda" and version "2.3.173.13" click on more options link
// * Click on Edit Configuration Parameters to launch the modal window
// * Click link Add Another Parameter
// * Enter the parameter name, parameter value and encrypt the parameter value and click on Cancel
// * Scroll to the package with name "StringUperCaseLambda" and version "2.3.173.13" click on more options link
// * Click on Edit Configuration Parameters to launch the modal window
// * Verify if added parameter name and parameter value on cancel does not exist
_____
* Close Browser