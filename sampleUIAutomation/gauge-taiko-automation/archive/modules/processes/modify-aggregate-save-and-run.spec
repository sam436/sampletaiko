# ModifyAggregate


Date Created        :
Version             : 1.0.0
Owner               : Agility Unite
Description         : Aggregate Process

table: resources/data/ModifyAggregate.csv



## Modify Aggregate
tags: AggregateProcess

* Open Browser
* Goto AU main page
* Click on processes link-Modify
* Enter <AggregateProcessName1> in Aggregate process-Modify
* Enter <sqlDescription> in Aggregate description box-Modify
* Enter <sqlDefinition> in Aggregate definition box-Modify
* Click on Schedule-Modify
* Click on radiobutton <Button><dailyHours><dailyMinutes>-Modify
//* Click on save
//* Click on processes link_modify
//* Enter  <AggregateProcessName1> in search box-Modify
//* Click on <AggregateProcessName1>-Modify
//* Enter <sqlDescription1> in Aggregate description box-Modify
//* Enter <sqlDefinition2> in Aggregate definition box-Modify1
//* Click on save&run
* Close Browser