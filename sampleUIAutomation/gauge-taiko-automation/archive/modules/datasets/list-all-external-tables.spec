# List ExternalTable

Date Created        :
Version             : 1.0.0
Owner               : Agility Unite
Description         : ExternalTable List 

* Open Browser

## List ExternalTable
tags: ExternalTable

* Goto AU main page
* Click on Data Modeling tab
* Click on "External Tables" left navigation tab
* Ensure externaltable header "Name" show up
* Ensure externaltable header "S3 Location" show up
* Ensure externaltable header "File Schema" show up
* Ensure externaltable header "Database" show up
* Ensure externaltable header "Database Schema" show up
* Ensure externaltable header "Created By" show up
* Ensure externaltable header "Created Date" show up
//* click on the Name to do sorting
//* click on the S3 Location to do sorting
//* click on the File Name to do sorting
//* click on the Database to do sorting
//* click on the Database Schema to do sorting
//* click on the Created By to do sorting
//* click on the Created Date to do sorting
//* Search Name in the Externaltable summary "test"
//* Check if Table name shows up in the search grid "test"


_____
* Close Browser
