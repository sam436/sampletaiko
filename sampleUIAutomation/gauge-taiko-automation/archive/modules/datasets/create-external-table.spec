# Create External Table 

Date Created        :
Version             : 1.0.0
Owner               : Agility Unite
Description         : External Table

table: resources/data/createExternalTable.csv

* Open Browser

## External Table Creation
tags: ExternalTable

* Goto AU main page
* Click on Data Modeling tab
* Click on "External Tables" left navigation tab
* Click Button "Add External Table"
_____
* Close Browser